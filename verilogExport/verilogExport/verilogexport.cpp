#include <QFile>
#include <QTextStream>
#include "verilogexport.h"

static char module_name[] = "Verilog Exporter";
static char module_version[] = "0.0.1";

char* get_name()
{
    return module_name;
}

char* get_version()
{
    return module_version;
}

bool export_data(const FontSettingsManager& font_data, const QString& path)
{
    qDebug() << module_name << " called to export data\n";
    QFile out_file(path);
    if (out_file.open(QFile::WriteOnly)) {
        QTextStream data_stream(&out_file);
        const auto bit_width = font_data.get_height()*font_data.get_width();
        auto font_chars = font_data.get_characters();
        data_stream << "parameter bit [0:" << (bit_width-1) << "] font_ram [0:255] = '{\n";
        for (int idx = 0; idx < 256; idx++) {
            if (font_chars.contains(QString::number(idx))) {
                qDebug() << "Char: " << idx << " code: " << font_chars[QString::number(idx)] << "\n";
                data_stream << "\t" << bit_width << "'h" << font_chars[QString::number(idx)].toString() << ",\n";
            } else {
                data_stream << "\t" << bit_width << "'h" << QString("0").repeated(bit_width) << ",\n";
            }
        }
        data_stream << "};";
        qDebug() << "Data exported\n";
        out_file.close();
        return true;
    }
    return false;
}
