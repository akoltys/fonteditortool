#ifndef VERILOGEXPORT_H
#define VERILOGEXPORT_H

#include "fontsettingsmanager.hpp"
#include "verilogExport_global.h"
#include <QJsonDocument>

extern "C" VERILOGEXPORT_EXPORT char* get_name();
extern "C" VERILOGEXPORT_EXPORT char* get_version();
//extern "C" VERILOGEXPORT_EXPORT bool export_data(const QJsonDocument& font_data);
extern "C" VERILOGEXPORT_EXPORT bool export_data(const FontSettingsManager& font_data, const QString& path);

#endif // VERILOGEXPORT_H
