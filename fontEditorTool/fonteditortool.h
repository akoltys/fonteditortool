#ifndef FONTEDITORTOOL_H
#define FONTEDITORTOOL_H

#include <QMainWindow>
#include <QJsonObject>
#include "fontexporter.h"
#include "fontsettingsmanager.hpp"

QT_BEGIN_NAMESPACE
namespace Ui { class fontEditorTool; }
QT_END_NAMESPACE

class fontEditorTool : public QMainWindow
{
    Q_OBJECT

public:
    fontEditorTool(QWidget *parent = nullptr);
    ~fontEditorTool();

private slots:
    void on_actionExit_triggered();

    void on_actionNew_triggered();

    void on_charEditor_cellClicked(int row, int column);

    void on_charEditor_cellEntered(int row, int column);

    void on_addCharacter_clicked();

    void on_actionSave_triggered();

    void on_actionOpen_triggered();

    void on_clearCharacter_clicked();

    void on_charCode_valueChanged(int code);

private:
    Ui::fontEditorTool *ui;
    bool cache_dirty = false;
    FontSettingsManager font_settings;
    FontExporter font_exporter;


    void add_font_to_view(const QString& char_code_str, const QString& bit_representation);
    void clear_char_editor(int rows, int cols);
};
#endif // FONTEDITORTOOL_H
