#ifndef FONTSETTINGSMANAGER_H
#define FONTSETTINGSMANAGER_H

#include <QFile>
#include <QFileDialog>
#include <QJsonDocument>
#include <QJsonObject>
#include <QMessageBox>
#include <QString>
#include <stdexcept>
#include <optional>

class FontSettingsManager
{
public:
    FontSettingsManager() = default;
    void save() {
        if (!this->cache_dirty) {
            return;
        }

        if (this->path.isEmpty()) {
            this->get_path();
            if (this->path.isEmpty()) {
                return;
            }
        }

        QJsonDocument doc(this->data);
        QFile file(this->path);
        if(file.open(QIODevice::WriteOnly)) {
            file.write(doc.toJson());
            this->cache_dirty = false;
            qDebug() << "File saved successfully\n";
            return;
        }
    }

    void open(const QString& path) {
        QFile file(path);
        if (file.open(QIODevice::ReadOnly)) {
            auto tmp_settings = file.readAll();
            QJsonDocument doc = QJsonDocument::fromJson(tmp_settings);
            if (!is_config_valid(doc.object())) {
                qDebug() << "Font config file corrupted";
                return;
            }
            this->data = doc.object();
            this->characters = this->data[S_CHARACTERS].toObject();
            qDebug() << "font_data: " << this->data << '\n';
            qDebug() << "font_characters: " << this->characters << '\n';
        } else {
            throw std::runtime_error("Cannot open file: " + path.toStdString());
        }
    }

    void reset() {
        this->data = QJsonObject();
        this->characters = QJsonObject();
        this->path = QString();
    }

    void set_name(const QString& name) {
        this->cache_dirty = true;
        this->data[S_NAME] = name;
    }

    void set_size(const uint8_t width, const uint8_t height) {
        this->cache_dirty = true;
        this->data[S_WIDTH] = width;
        this->data[S_HEIGHT] = height;
    }

    void set_char(const QString& code, const QString& bits) {
        if (this->characters.contains(code)) {
            auto ret = QMessageBox::question(nullptr, "Overwrite char", "Do you want to overwrite character code " + code);
            if (ret == QMessageBox::No) {
                qDebug() << "Cancel overwriting char code " + code;
                return;
            }
            qDebug() << "Overwriting char code " << code;
        }
        this->cache_dirty = true;
        this->characters[code] = bits;
        this->data[S_CHARACTERS] = this->characters;
        return;
    }

    std::optional<QString> get_char(const QString& code) {
        if (this->characters.contains(code)) {
            return this->characters[code].toString();
        }
        return {};
    }

    int get_width() const {
        return this->data[S_WIDTH].toInt();
    }

    int get_height() const {
        return this->data[S_HEIGHT].toInt();
    }

    const QJsonObject& get_characters() const {
        return this->characters;
    }

private:
    void get_path() {
        auto tmp_path = QFileDialog::getSaveFileName(nullptr, "Font settings path", QString(), "Font settings (*.json)");
        if (tmp_path.isEmpty()) {
            qDebug() << "User did not provide file name\n";
            return;
        }
        if (!tmp_path.endsWith(".json")) {
            tmp_path += ".json";
        }
        this->path = tmp_path;
    }
    bool is_config_valid(const QJsonObject& config) const {
        if (!config.contains(S_NAME)) {
            return false;
        }
        if (!config.contains(S_HEIGHT)) {
            return false;
        }
        if (!config.contains(S_WIDTH)) {
            return false;
        }
        if (!config.contains(S_CHARACTERS)) {
            return false;
        }
        return true;
    }

    const QString S_NAME = "name";
    const QString S_WIDTH = "width";
    const QString S_HEIGHT = "height";
    const QString S_CHARACTERS = "characters";
    QString path;
    QJsonObject data;
    QJsonObject characters;
    bool cache_dirty = false;

};

#endif // FONTSETTINGSMANAGER_H
