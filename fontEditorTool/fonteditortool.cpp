#include <QFile>
#include <QFileDialog>
#include <QJsonDocument>
#include <QLibrary>
#include <QMessageBox>
#include "fonteditortool.h"
#include "fontpreview.h"
#include "newfontwindow.h"
#include "./ui_fonteditortool.h"

fontEditorTool::fontEditorTool(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::fontEditorTool)
    , font_exporter(QString("./plugins"), font_settings)
{
    ui->setupUi(this);
    font_exporter.addPlugins(ui->menuExport);
}

fontEditorTool::~fontEditorTool()
{
    delete ui;
}


void fontEditorTool::on_actionExit_triggered()
{
    qDebug() << "Exit button pressed\n";
    if (this->cache_dirty) {
        // Warn about unsaved changes
        auto ret = QMessageBox::question(this, "Unsaved progress", "You have unsaved changes, do you want to discard those changes and exit?");
        if (ret == QMessageBox::No) {
            qDebug() << "Not closing editor because of unsaved changes";
            return;
        }
    }
    this->close();
}

void fontEditorTool::on_actionSave_triggered()
{
    this->font_settings.save();
}

void fontEditorTool::on_actionOpen_triggered()
{
    qDebug() << "Open font settings\n";
    auto tmp_path = QFileDialog::getOpenFileName(this, "Font settings path", QString(), tr("Font settings (*.json)"));
    qDebug() << "Opening font file: " << tmp_path << '\n';
    try {
        this->font_settings.open(tmp_path);
    } catch (std::exception& e) {
        QMessageBox::warning(this, "Failed to open config", "Could not open config file.");
        return;
    }
    auto tmp_chars = this->font_settings.get_characters();
    for (auto tmp_char: tmp_chars.keys()) {
        qDebug() << "Char: " << tmp_char << " Val: " << tmp_chars[tmp_char].toString() << '\n';
        add_font_to_view(tmp_char, tmp_chars[tmp_char].toString());
    }
    clear_char_editor(this->font_settings.get_height(), this->font_settings.get_width());
}

void fontEditorTool::on_actionNew_triggered()
{
    qDebug() << "New button pressed\n";
    NewFontWindow newFont(this);
    if(newFont.exec()){
        auto settings = newFont.getSettings();
        qDebug() << "Font name: " << settings.name << " width: " << settings.width << " height: " << settings.height << '\n';
        this->font_settings.reset();
        this->font_settings.set_name(settings.name);
        this->font_settings.set_size(settings.width, settings.height);
        clear_char_editor(settings.height, settings.width);
    }
    qDebug() << "New button handler finished\n";
}


void fontEditorTool::on_charEditor_cellClicked(int row, int column)
{
    qDebug() << "User clicked on cell " << row << " : " << column << '\n';
    auto cell = this->ui->charEditor->item(row, column);
    if (cell == nullptr) {
        qDebug() << "Invalid cell position!\n";
        return;
    }
    qDebug() << "Changing old cell background color on cell: " << (uint64_t)cell << '\n';
    auto backgroundColor = cell->background();
    if (backgroundColor.color().rgb() == qRgb(255,255,255)) {
        qDebug() << "Change color to black\n";
        QBrush newColor(QColor(0,0,0));
        cell->setBackground(newColor);
    } else {
        qDebug() << "Change color to white\n";
        QBrush newColor(QColor(255,255,255));
        cell->setBackground(newColor);
    }
}


void fontEditorTool::on_charEditor_cellEntered(int row, int column)
{
    qDebug() << "User entered cell " << row << " : " << column << '\n';
    auto cell = this->ui->charEditor->item(row, column);
    if (cell == nullptr) {
        qDebug() << "Invalid cell position!\n";
        return;
    }
    qDebug() << "Changing old cell background color on cell: " << (uint64_t)cell << '\n';
    auto backgroundColor = cell->background();
    if (backgroundColor.color().rgb() == qRgb(255,255,255)) {
        qDebug() << "Change color to black\n";
        QBrush newColor(QColor(0,0,0));
        cell->setBackground(newColor);
    } else {
        qDebug() << "Change color to white\n";
        QBrush newColor(QColor(255,255,255));
        cell->setBackground(newColor);
    }
}


void fontEditorTool::on_addCharacter_clicked()
{
    qDebug() << "addCharacter button pressed\n";
    const auto char_code = this->ui->charCode->value();
    const auto char_code_str = QString("0x%1").arg(char_code, 2, 16, QLatin1Char('0'));//QString::number(char_code);
    const auto bit_representation = [&]() -> QString {
        QString result;
        for (auto row = 0; row < this->ui->charEditor->rowCount(); row++) {
            for (auto col = 0; col < this->ui->charEditor->columnCount(); col++) {
                result += (this->ui->charEditor->item(row, col)->background().color().rgb() == qRgb(0,0,0)) ? "1" : "0";
            }
        }
        return result;
    }();

    this->font_settings.set_char(char_code_str, bit_representation);
    add_font_to_view(char_code_str, bit_representation);
}

void fontEditorTool::add_font_to_view(const QString& char_code_str, const QString& bit_representation)
{
    if (!this->ui->font_list->findItems(char_code_str, Qt::MatchExactly).isEmpty()) {
        return;
    }

    qDebug() << "New preview widget creating...";
    FontPreview* tmp_preview = new FontPreview(this, bit_representation, char_code_str, this->font_settings.get_width(), this->font_settings.get_height());
    qDebug() << "New preview widget created";
    auto tmp_item = new QListWidgetItem();
    tmp_item->setSizeHint(tmp_preview->sizeHint());
    tmp_item->setText(char_code_str);
    this->ui->font_list->addItem(tmp_item);
    this->ui->font_list->setItemWidget(tmp_item, tmp_preview);
    this->ui->font_list->sortItems();
}

void fontEditorTool::clear_char_editor(int rows, int cols)
{
    this->ui->charEditor->clearContents();
    this->ui->charEditor->setRowCount(rows);
    this->ui->charEditor->setColumnCount(cols);
    for (auto ridx = 0; ridx < rows; ridx++) {
        for (auto cidx = 0; cidx < cols; cidx++) {
            auto cell = new QTableWidgetItem();
            cell->setFlags(cell->flags() & ~Qt::ItemFlag::ItemIsEditable);
            QBrush newColor(QColor(255,255,255));
            cell->setBackground(newColor);
            this->ui->charEditor->setItem(ridx, cidx, cell);
        }
    }
}

void fontEditorTool::on_clearCharacter_clicked()
{
    const QBrush new_color(QColor(255,255,255));
    for (auto row = 0; row < this->ui->charEditor->rowCount(); row++) {
        for (auto col = 0; col < this->ui->charEditor->columnCount(); col++) {
            this->ui->charEditor->item(row, col)->setBackground(new_color);
        }
    }
}


void fontEditorTool::on_charCode_valueChanged(int code)
{
    const auto char_code_str = QString("0x%1").arg(code, 2, 16, QLatin1Char('0'));
    qDebug() << "charCode_valueChanged: " << char_code_str << '\n';
    const auto bit_representation = this->font_settings.get_char(char_code_str);
    qDebug() << "Bit representation: " << bit_representation.value_or("No value!") << '\n';
    if (bit_representation) {
        for (auto row = 0; row < this->ui->charEditor->rowCount(); row++) {
            for (auto col = 0; col < this->ui->charEditor->columnCount(); col++) {
                auto idx = row*this->ui->charEditor->columnCount()+col;
                if (bit_representation.value()[idx] != '0') {
                    QBrush newColor(QColor(0,0,0));
                    this->ui->charEditor->item(row, col)->setBackground(newColor);
                } else {
                    QBrush newColor(QColor(255,255,255));
                    this->ui->charEditor->item(row, col)->setBackground(newColor);
                }
            }
        }
    }
}

