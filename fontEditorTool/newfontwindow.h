#ifndef NEWFONTWINDOW_H
#define NEWFONTWINDOW_H

#include <QDialog>

namespace Ui {
class NewFontWindow;
}

struct NewFontSettings {
    QString name;
    uint8_t width;
    uint8_t height;
};

class NewFontWindow : public QDialog
{
    Q_OBJECT

public:
    explicit NewFontWindow(QWidget *parent = nullptr);
    ~NewFontWindow();
    NewFontSettings getSettings();

private:
    Ui::NewFontWindow *ui;
};

#endif // NEWFONTWINDOW_H
