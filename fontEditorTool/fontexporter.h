#ifndef FONTEXPORTER_H
#define FONTEXPORTER_H

#include <map>
#include <QJsonDocument>
#include <QMenu>
#include "fontsettingsmanager.hpp"

class FontExporter : QObject
{
public:
    FontExporter(QString&& path, FontSettingsManager& settings);
    void addPlugins(QMenu* menu);

private:
    typedef char* (*name_func)();
    typedef char* (*version_func)();
    typedef bool (*export_func)(const FontSettingsManager& font_data, const QString& path);
    struct Exporter {
        QString version;
        export_func exports;
    };
    std::map<QString, Exporter> export_plugins;
    FontSettingsManager& font_settings;

    void load_export_libs(const QString& path);
    void load_export_lib(const QString& path);
    void on_export_clicked(const QString& plugin);

};

#endif // FONTEXPORTER_H
