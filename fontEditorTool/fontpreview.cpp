#include "fontpreview.h"
#include "ui_fontpreview.h"

FontPreview::FontPreview(QWidget *parent, const QString& bit_representation, const QString& code, const uint8_t column_count, const uint8_t row_count) :
    QWidget(parent),
    ui(new Ui::FontPreview)
{
    ui->setupUi(this);
    if (!bit_representation.isEmpty()) {
        qDebug() << "Creating preview table";
        ui->font_preview->setColumnCount(column_count);
        ui->font_preview->setRowCount(row_count);
        ui->font_preview->setMaximumWidth(5 * column_count+1);
        ui->font_preview->setMaximumHeight(5 * row_count+1);
        for (auto row = 0; row < row_count; row++) {
            for (auto col = 0; col < column_count; col++) {
                auto cell = new QTableWidgetItem();
                cell->setFlags(cell->flags() & ~Qt::ItemFlag::ItemIsEditable);
                const auto bit = bit_representation.at(row * column_count + col) == '1' ? 0 : 1;
                QBrush background(QColor(0xff*bit, 0xff*bit, 0xff*bit));
                cell->setBackground(background);
                this->ui->font_preview->setItem(row, col, cell);
            }
        }
        ui->font_code->setText("**"+code+"**");
    }
}

FontPreview::~FontPreview()
{
    delete ui;
}
