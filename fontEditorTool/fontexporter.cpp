#include <QDir>
#include <QLibrary>
#include "fontexporter.h"

FontExporter::FontExporter(QString&& path, FontSettingsManager& settings) : font_settings(settings)
{
    load_export_libs(path);
}

void FontExporter::addPlugins(QMenu *menu)
{
    for (auto plugin: export_plugins) {
        QAction *tmp_action = new QAction();
        tmp_action->setText(plugin.first);
        connect(tmp_action, &QAction::triggered, this, [this, plugin] {
            on_export_clicked(plugin.first);
        });
        menu->addAction(tmp_action);
    }
}

void FontExporter::on_export_clicked(const QString &plugin)
{
    qDebug() << "Export plugin: " << plugin << " clicked\n";
    this->export_plugins[plugin].exports(font_settings, "./tmp_file.v");
}

void FontExporter::load_export_libs(const QString &path)
{
    QDir lib_dir(path);
    auto libs = lib_dir.entryInfoList(QStringList() << "*.so" << "*.dll", QDir::Files);
    for (auto lib: libs) {
        qDebug() << "Loaded lib: " << lib.filesystemAbsoluteFilePath() << '\n';
        load_export_lib(lib.absoluteFilePath());
    }
}

void FontExporter::load_export_lib(const QString &path)
{
    QLibrary tmp_lib(path);
    qDebug() << "Lib loaded: " << tmp_lib.load() << '\n';
    name_func tmp_get_name = (name_func)tmp_lib.resolve("get_name");
    version_func tmp_get_version = (version_func)tmp_lib.resolve("get_version");
    export_func tmp_export = (export_func)tmp_lib.resolve("export_data");
    qDebug() << "Load test done. n: " << tmp_get_name << " v: " << tmp_get_version << " e: " << tmp_export << '\n';
    qDebug() << "Plugin name: " << tmp_get_name() << '\n';
    qDebug() << "Plugin ver:  " << tmp_get_version() << '\n';
    Exporter tmp_plugin = {QString(tmp_get_version()), tmp_export};
    this->export_plugins[QString(tmp_get_name())] = tmp_plugin;
}
