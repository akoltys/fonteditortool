#include "newfontwindow.h"
#include "./ui_newfontwindow.h"

NewFontWindow::NewFontWindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::NewFontWindow)
{
    ui->setupUi(this);
}

NewFontWindow::~NewFontWindow()
{
    delete ui;
}

NewFontSettings NewFontWindow::getSettings()
{
    NewFontSettings result;
    result.name = this->ui->fontName->text();
    result.width = this->ui->fontWidth->value();
    result.height = this->ui->fontHeight->value();
    return result;
}
