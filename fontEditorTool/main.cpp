#include "fonteditortool.h"

#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    fontEditorTool w;
    w.show();
    return a.exec();
}
