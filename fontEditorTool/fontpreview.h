#ifndef FONTPREVIEW_H
#define FONTPREVIEW_H

#include <QWidget>
#include <QTableWidget>

namespace Ui {
class FontPreview;
}

class FontPreview : public QWidget
{
    Q_OBJECT

public:
    explicit FontPreview(QWidget *parent = nullptr, const QString& bit_representation = QString(), const QString& code = QString(), const uint8_t column_count = 0, const uint8_t row_count = 0);
    ~FontPreview();

private:
    Ui::FontPreview *ui;
};

#endif // FONTPREVIEW_H
